# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from .handlers import MessageArchivedHandler
from minty_amqp.consumer import BaseConsumer


class DocumentsEventsConsumer(BaseConsumer):
    def _register_routing(self):
        self._known_handlers = [MessageArchivedHandler(self.cqrs)]

        self.routing_keys = []
        for handler in self._known_handlers:
            self.routing_keys.extend(handler.routing_keys)
